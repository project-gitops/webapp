FROM nginx

ARG AUTHOR=menechini

COPY index.html /usr/share/nginx/html

RUN sed -i.bak 1s/AUTHOR/${AUTHOR}/ /usr/share/nginx/html/index.html
